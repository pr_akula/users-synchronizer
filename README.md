## Launching instruction

Firstly download the project from remote GitLab repository via command:
> git clone git@gitlab.com:pr_akula/users-synchronizer.git [press Enter]

After add new database in your local / remote server. For example, in Linux for MySQL you can do it in terminal via commands:
> mysql -u <mysql_user_name> -p [press Enter]
> >create database user_synchronizer_db; [press Enter]
> >
> >\q [press Enter for quitting]

After add the .env file inside your project if it's not exists and add the database configuration inside it.

After download the dependencies (packages) for your Laravel 9 project:
> composer install [press Enter]

I recommend to use docker for containerization of your project. I use Laravel build-in packages for its goals recommended by official docs.
Type in your terminal:
> alias sail='[ -f sail ] && bash sail || bash vendor/bin/sail' [press Enter]
>
> sail up -d [press Enter]
And use:
> sail stop [press Enter]
for stopping.

After run database migration:
> sail php artisan migrate [press Enter]

After seed the data into the database:
> sail php artisan db:seed [press Enter]

And for the health of a client-server application testing, use commands below:
> sail php artisan dummy:users:load [press Enter]
> 
> sail php artisan dummy:users:synchronize 1 [press Enter]
> 
> sail php artisan dummy:users:user:update {Dummy API user ID}
--about={mr|mrs|ms|miss} --firstname={some firstname} --lastname={some lastname} --email={some E-Mail} [press Enter]
> 
> sail php artisan dummy:users:user:delete {Dummy API user ID} [press Enter]

and show the content of the users table inside your database.
