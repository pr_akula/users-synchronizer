<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Contracts\MySQL\System\UserTableContract as Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * @return void
     */
    final public function run(): void
    {
        $data = [
            ['email' => 'epolyglotov@gmail.com', 'firstname' => 'Egor', 'lastname' => 'Polyglotov',
                'phone' => '+79250004215', 'about' => 'mr',],
            ['email' => 'agafya_krysler@gmail.com', 'firstname' => 'Agafya', 'lastname' => 'Krysler',
                'phone' => '+79268342424', 'about' => 'mrs',],
            ['email' => 'a_zelimhanov@gmail.com', 'firstname' => 'Anatoly', 'lastname' => 'Zelimhanov',
                'phone' => '+79141717171', 'about' => 'mr',],
            ['email' => 'olganahimova@gmail.com', 'firstname' => 'Olga', 'lastname' => 'Nahimova',
                'phone' => '+79081662636', 'about' => 'ms',],
            ['email' => 'avelikanov@gmail.com', 'firstname' => 'Andrey', 'lastname' => 'Velikanov',
                'phone' => '+77078002020', 'about' => 'mr',],
            ['email' => 'rada_kunitsyna@gmail.com', 'firstname' => 'Rada', 'lastname' => 'Kunitsyna',
                'phone' => '+77779997979', 'about' => 'ms',],
            ['email' => 'rus_bronevoy@gmail.com', 'firstname' => 'Ruslan', 'lastname' => 'Bronevoy',
                'phone' => '+77473246776', 'about' => 'mr',],
            ['email' => 'e_semyonova@gmail.com', 'firstname' => 'Eugeniya', 'lastname' => 'Semyonova',
                'phone' => '+79992845128', 'about' => 'mrs',],
            ['email' => 'timur_valunov@gmail.com', 'firstname' => 'Timur', 'lastname' => 'Valunov',
                'phone' => '+77016006060', 'about' => 'mr',],
            ['email' => 'zhemchuzhnaya@gmail.com', 'firstname' => 'Margarita', 'lastname' => 'Zhemchuzhnaya',
                'phone' => '+77024277115', 'about' => 'ms',],
            ['email' => 'saraandersen@gmail.com', 'firstname' => 'Sara', 'lastname' => 'Andersen',
                'external_id' => '60d0fe4f5311236168a109ca', 'phone' => '+14845828891', 'about' => 'ms',],
            ['email' => 'adinabarbosa@gmail.com', 'firstname' => 'Adina', 'lastname' => 'Barbosa',
                'external_id' => '60d0fe4f5311236168a109cc', 'phone' => '+16102883541', 'about' => 'ms',],
            ['email' => 'leevisavela@gmail.com', 'firstname' => 'Leevi', 'lastname' => 'Savela',
                'external_id' => '60d0fe4f5311236168a109d7', 'phone' => '+15857042179', 'about' => 'mr',],
            ['email' => 'kyleflanter@gmail.com', 'firstname' => 'Kyle', 'lastname' => 'Flanter',
                'external_id' => '60d0fe4f53112370716a032c', 'phone' => '+15857241003', 'about' => 'mr',],
        ];

        foreach ($data as $value) {
            $currentDateTime = new \DateTime('now');
            DB::table(Storage::TABLE_NAME)->insert([
                'email' => $value['email'],
                'email_verified_at' => $currentDateTime,
                'password' => Hash::make($value['email']),
                'firstname' => $value['firstname'],
                'lastname' => $value['lastname'],
                'phone' => $value['phone'],
                'phone_verified_at' => $currentDateTime,
                'external_id' => $value['external_id'] ?? null,
                'about' => $value['about'],
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ]);
        }
    }
}
