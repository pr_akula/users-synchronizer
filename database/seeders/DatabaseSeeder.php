<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * @return void
     */
    final public function run(): void
    {
        $this->call([
            UserSeeder::class,
        ]);
    }
}
