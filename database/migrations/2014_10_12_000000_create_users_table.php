<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Contracts\MySQL\System\UserTableContract as Storage;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(Storage::TABLE_NAME)) {
            Schema::create(Storage::TABLE_NAME, function (Blueprint $table) {
                $table->id();
                $table->string(Storage::EMAIL, 127)->nullable()->comment('E-Mail');
                $table->timestamp(Storage::EMAIL_VERIFIED_AT)->nullable()->comment('E-Mail verified at ');
                $table->string(Storage::PASSWORD, 511)->nullable()->comment('Password');

                $table->string(Storage::FIRSTNAME, 63)->nullable()->comment('Firstname');
                $table->string(Storage::MIDDLENAME, 63)->nullable()->comment('Middlename');
                $table->string(Storage::LASTNAME, 63)->nullable()->comment('Lastname');

                $table->string(Storage::PHONE, 15)->nullable()->comment('Phone');
                $table->timestamp(Storage::PHONE_VERIFIED_AT)->nullable()->comment('Phone verified at ');
                $table->string(Storage::EXTERNAL_ID, 24)->nullable()->comment('External identificator');

                $table->json(Storage::MIDDLEWARES, 511)->nullable()->comment('Middlewares');

                $table->date(Storage::BIRTHDATE)->nullable()->comment('Birth date');
                $table->string(Storage::ABOUT, 511)->nullable()->comment('About');
                $table->string(Storage::ADDRESS, 511)->nullable()->comment('Address');
                $table->string(Storage::ORGANIZATION, 63)->nullable()->comment('Organization');

                $table->rememberToken();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Storage::TABLE_NAME);
    }
};
