<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Contracts\MySQL\JobSystem\JobTableContract as Storage;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(Storage::TABLE_NAME)) {
            Schema::create(Storage::TABLE_NAME, function (Blueprint $table) {
                $table->id();

                $table->string(Storage::NAME, 255)->comment('Name');
                $table->string(Storage::DESCRIPTION, 1023)->nullable()->comment('Description');
                $table->string(Storage::STATUS, 15)->nullable()->comment('Status');

                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Storage::TABLE_NAME);
    }
};
