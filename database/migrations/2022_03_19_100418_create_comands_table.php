<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Contracts\MySQL\JobSystem\CommandTableContract as Storage;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(Storage::TABLE_NAME)) {
            Schema::create(Storage::TABLE_NAME, function (Blueprint $table) {
                $table->id();

                $table->unsignedInteger(Storage::JOB_FOREIGN_KEY)->comment('Job ID');
                $table->unsignedTinyInteger(Storage::QUEUE_NUMBER)->comment('Queue number');
                $table->json(Storage::EXECUTE_CONFIG)->comment('Execute config');
                $table->string(Storage::STATUS, 15)->comment('Status');
                $table->longText(Storage::RESPONSE_DATA)->nullable()->comment('Response data');
                $table->longText(Storage::ERROR_MESSAGE)->nullable()->comment('Error message');

                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Storage::TABLE_NAME);
    }
};
