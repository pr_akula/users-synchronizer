<?php

namespace App\Models\JobSystem;

use App\Contracts\MySQL\JobSystem\CommandTableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Artisan;

class Command extends Model implements CommandTableContract
{
    use HasFactory;

    protected $fillable = [
        self::JOB_FOREIGN_KEY,
        self::QUEUE_NUMBER,
        self::EXECUTE_CONFIG,
        self::STATUS,
        self::RESPONSE_DATA,
        self::ERROR_MESSAGE,
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function job(): BelongsTo
    {
        return $this->belongsTo(Job::class, self::JOB_FOREIGN_KEY, Job::PRIMARY_KEY);
    }
}
