<?php

namespace App\Models\JobSystem;

use App\Contracts\MySQL\JobSystem\JobTableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Job extends Model implements JobTableContract
{
    use HasFactory;

    protected $fillable = [
        self::NAME,
        self::DESCRIPTION,
        self::STATUS,
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function commands(): HasMany
    {
        return $this->hasMany(Command::class, Command::JOB_FOREIGN_KEY, self::PRIMARY_KEY);
    }
}
