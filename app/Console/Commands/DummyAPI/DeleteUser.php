<?php

namespace App\Console\Commands\DummyAPI;

use App\Enums\JobSystem\CommandStatus;
use App\Enums\JobSystem\JobStatus;
use App\Integrations\DummyAPI\DummyApiClient;
use App\Models\JobSystem\Job;
use App\Models\JobSystem\Command;
use App\Models\System\User;
use Exception;
use Illuminate\Console\Command as ConsoleCommand;

class DeleteUser extends ConsoleCommand
{
    /**
     * @var string
     */
    protected $signature = 'dummy:users:user:delete {id : External ID - Dummy API user ID}';

    /**
     * @var string
     */
    protected $description = 'Delete user in Dummy API and synchronize the deleting with internal system';

    public function __construct(
        private DummyApiClient $apiClient = new DummyApiClient(),
    )
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    final public function handle(): int
    {
        $deletedUserId = $this->deleteUser();

        echo 'The user with ID ' . $deletedUserId . ' was deleted in Dummy API system and in internal system.';

        return 0;
    }

    private function deleteUser(): string
    {
        $externalId = $this->argument('id');
        $user = User::where(User::EXTERNAL_ID, '=', $externalId)->first();
        if ($user) {
            $response = $this->apiClient->deleteUser(id: $externalId);
            if ($response->getStatusCode() == 200) {
                $content = json_decode($response->getBody()->getContents(), true);

                $externalId = $content['id'];
                User::where(User::EXTERNAL_ID, '=', $externalId)->delete();

                return $externalId;
            }

            return $user;
        } else {
            throw new Exception('The user with ID ' . $externalId . ' not found in the system.');
        }
    }
}
