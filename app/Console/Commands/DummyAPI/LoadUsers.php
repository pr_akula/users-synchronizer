<?php

namespace App\Console\Commands\DummyAPI;

use App\Integrations\DummyAPI\DummyApiClient;
use App\Models\System\User;
use Illuminate\Console\Command;

class LoadUsers extends Command
{
    /**
     * @var string
     */
    protected $signature = 'dummy:users:load:pagely {page=0 : The number of page}';

    /**
     * @var string
     */
    protected $description = 'Load users from Dummy API';

    /**
     * @param DummyApiClient $apiClient
     */
    public function __construct(private DummyApiClient $apiClient = new DummyApiClient())
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    final public function handle(): int
    {
        $page = $this->argument('page');

        $response = $this->apiClient->getUserList(page: $page);
        $content = json_decode($response->getBody()->getContents(), true);
        foreach ($content['data'] as $item) {
            $user = User::where(User::EXTERNAL_ID, $item['id'])->first();
            if (!$user || ($user && !$this->isUserDataMatching($user, $item))) {
                $user = new User;
                $user->{User::EXTERNAL_ID} = $item['id'];
                $user->{User::ABOUT} = $item['title'];
                $user->{User::FIRSTNAME} = $item['firstName'];
                $user->{User::LASTNAME} = $item['lastName'];

                $user->save();
            }
        }


        return 0;
    }

    private function isUserDataMatching(User $user, array $userData): bool
    {
        if ($user->{User::EXTERNAL_ID} != $userData['id']) {
            return false;
        }
        if ($user->{User::ABOUT} != $userData['title']) {
            return false;
        }
        if ($user->{User::FIRSTNAME} != $userData['firstName']) {
            return false;
        }
        if ($user->{User::LASTNAME} != $userData['lastName']) {
            return false;
        }

        return true;
    }
}
