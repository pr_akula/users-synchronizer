<?php

namespace App\Console\Commands\DummyAPI;

use App\Enums\JobSystem\CommandStatus;
use App\Enums\JobSystem\JobStatus;
use App\Integrations\DummyAPI\DummyApiClient;
use App\Models\JobSystem\Job;
use App\Models\JobSystem\Command;
use App\Models\System\User;
use Exception;
use Illuminate\Console\Command as ConsoleCommand;

class SynchronizeLoadedUsers extends ConsoleCommand
{
    /**
     * @var string
     */
    protected $signature = 'dummy:users:synchronize {job : The ID of the job}';

    /**
     * @var string
     */
    protected $description = 'Synchronize loaded users from Dummy API with internal users';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    final public function handle(): int
    {
        $jobId = $this->argument('job');
        $job = Job::findOrFail($jobId);

        $loadedExternalUsers = [];
        foreach ($job->commands as $command) {
            $responseData = json_decode($command->{Command::RESPONSE_DATA}, true);

            $loadedExternalUsers = array_merge($loadedExternalUsers, $responseData['data']);
        }

        $existingExternalUsers = User::where(User::EXTERNAL_ID, '<>', null)->get();

        $actualExternalIds = [];
        foreach ($loadedExternalUsers as $item) {
            $actualExternalIds[] = $item['id'];

            $sameUser = User::where(User::EXTERNAL_ID, $item['id'])->first();
            if (!$sameUser || ($sameUser && self::isUserDataMatching($sameUser, $item))) {
                $user = new User;
                $user->{User::EXTERNAL_ID} = $item['id'];
                $user->{User::ABOUT} = $item['title'] ?? null;
                $user->{User::FIRSTNAME} = $item['firstName'] ?? null;
                $user->{User::LASTNAME} = $item['lastName'] ?? null;

                $user->save();
            }
        }

        foreach ($existingExternalUsers as $user) {
            if (!in_array($user->{User::EXTERNAL_ID}, $actualExternalIds)) {
                $user->delete();
            }
        }

        return 0;
    }

    private static function isUserDataMatching(User $user, array $userData): bool
    {
        if ($user->{User::EXTERNAL_ID} != $userData['id']) {
            return false;
        }
        if ($user->{User::ABOUT} != $userData['title']) {
            return false;
        }
        if ($user->{User::FIRSTNAME} != $userData['firstName']) {
            return false;
        }
        if ($user->{User::LASTNAME} != $userData['lastName']) {
            return false;
        }

        return true;
    }
}
