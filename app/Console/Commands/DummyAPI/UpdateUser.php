<?php

namespace App\Console\Commands\DummyAPI;

use App\Enums\JobSystem\CommandStatus;
use App\Enums\JobSystem\JobStatus;
use App\Integrations\DummyAPI\DummyApiClient;
use App\Models\JobSystem\Job;
use App\Models\JobSystem\Command;
use App\Models\System\User;
use Exception;
use Illuminate\Console\Command as ConsoleCommand;

class UpdateUser extends ConsoleCommand
{
    /**
     * @var string
     */
    protected $signature = 'dummy:users:user:update {id : External ID - Dummy API user ID}
                                                    {--E|email=? : Value for changing "email" field}
                                                    {--A|about=? : Value for changing "about" field}
                                                    {--F|firstname=? : Value for changing "firstname" field}
                                                    {--L|lastname=? : Value for changing "lastname" field}';

    /**
     * @var string
     */
    protected $description = 'Update user in Dummy API and synchronize the updating with internal system';

    public function __construct(
        private DummyApiClient $apiClient = new DummyApiClient(),
    )
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    final public function handle(): int
    {
        $updatedUser = $this->updateUser();

        echo 'The user with ID ' . $updatedUser->{User::PRIMARY_KEY}
            . ' was updated in Dummy API system and in internal system.';

        return 0;
    }

    private function updateUser(): User
    {
        $externalId = $this->argument('id');
        $user = User::where(User::EXTERNAL_ID, '=', $externalId)->first();
        if ($user) {
            $about = empty($this->option('about')) ? $user->about : $this->option('about');
            $email = empty($this->option('email')) ? $user->email : $this->option('email');
            $firstname = empty($this->option('firstname')) ? $user->firstname : $this->option('firstname');
            $lastname = empty($this->option('lastname')) ? $user->lastname : $this->option('lastname');

            $response = $this->apiClient->updateUser(
                id: $externalId,
                email: $email,
                title: $about,
                firstName: $firstname,
                lastName: $lastname,
            );
            if ($response->getStatusCode() == 200) {
                $content = json_decode($response->getBody()->getContents(), true);

                $user->about = $content['title'] ?? $user->about;
                $user->email = $content['email'] ?? $user->email;
                $user->firstname = $content['firstName'] ?? $user->firstname;
                $user->lastname = $content['lastName'] ?? $user->lastname;
                $user->save();
            }

            return $user;
        } else {
            throw new Exception('The user with ID ' . $externalId . ' not found in the system.');
        }
    }
}
