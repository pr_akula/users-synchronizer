<?php

namespace App\Console\Commands\DummyAPI;

use App\Enums\JobSystem\CommandStatus;
use App\Enums\JobSystem\JobStatus;
use App\Integrations\DummyAPI\DummyApiClient;
use App\Models\JobSystem\Job;
use App\Models\JobSystem\Command;
use Exception;
use Illuminate\Console\Command as ConsoleCommand;

class LoadUserList extends ConsoleCommand
{
    /**
     * @var string
     */
    protected $signature = 'dummy:users:load';

    /**
     * @var string
     */
    protected $description = 'Load users from Dummy API';

    public function __construct(
        private DummyApiClient $apiClient = new DummyApiClient(),
    )
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    final public function handle(): int
    {
        $job = $this->init();
        $this->executeCommands($job);

        return 0;
    }

    private function init(): Job
    {
        $job = new Job([
            Job::NAME => 'Load Dummy API users #' . (new \DateTime('now'))->format('Y-m-d H:i:s'),
            Job::DESCRIPTION => 'Complex loading of the users from Dummy API to internal storage in several steps.',
            Job::STATUS => JobStatus::ADDED,
        ]);
        $job->save();

        $response = call_user_func_array([$this->apiClient, 'getUserList'], []);
        $content = json_decode($response->getBody()->getContents(), true);
        $pagesCount = $this->calculatePagesCount($content['total']);

        for ($pageCount = 0; $pageCount < $pagesCount; $pageCount++) {
            $command = new Command([
                Command::QUEUE_NUMBER => $pageCount + 1,
                Command::EXECUTE_CONFIG => json_encode([
                    'run_class' => DummyApiClient::class,
                    'run_method' => 'getUserList',
                    'run_method_parameters' => ['page' => $pageCount],

                    'prepare_result_class' => DummyApiClient::class,
                    'prepare_result_method' => 'stringifyResponse',
                ], true),
                Command::STATUS => CommandStatus::ADDED,
                Command::RESPONSE_DATA => null,
            ]);

            $job->commands()->save($command);
        }

        return $job;
    }

    private function executeCommands(Job $job): Job
    {
        foreach ($job->commands as $command) {
            $executeConfig = json_decode($command->{Command::EXECUTE_CONFIG}, true);
            $runClass = $executeConfig['run_class'];
            $runMethod = $executeConfig['run_method'];
            $runMethodParameters = $executeConfig['run_method_parameters'];
            $prepareResultClass = $executeConfig['prepare_result_class'];
            $prepareResultMethod = $executeConfig['prepare_result_method'];

            try {
                $result = call_user_func_array([new $runClass(), $runMethod], $runMethodParameters);
                $preparedResult = call_user_func_array([$prepareResultClass, $prepareResultMethod], [$result]);
                $command->{Command::RESPONSE_DATA} = $preparedResult;
                $command->{Command::STATUS} = CommandStatus::DATA_STORED;
                $command->save();
            } catch (Exception $exception) {
                $command->{Command::ERROR_MESSAGE} = $exception;
                $command->{Command::STATUS} = CommandStatus::DATA_NOT_RECEIVED;
                $command->save();
            }
        }

        return $job;
    }

    private function calculatePagesCount(float|int $total): int
    {
        $limit = (int) env('DUMMY_API_LIMIT');
        $integerDivisionCount = (int) $total / $limit;
        $fractionalDivisionCount = (int) $total % $limit === 0 ? 0 : 1;

        return $integerDivisionCount + $fractionalDivisionCount;
    }
}
