<?php

namespace App\Console\Commands\DummyAPI;

use App\Integrations\DummyAPI\DummyApiClient;
use App\Models\System\User;
use Illuminate\Console\Command;

class SynchronizeUsers extends Command
{
    /**
     * @var string
     */
    protected $signature = 'dummy:users:synchronize11';

    /**
     * @var string
     */
    protected $description = 'Synchronize users with Dummy API';

    /**
     * @param DummyApiClient $apiClient
     */
    public function __construct(private DummyApiClient $apiClient = new DummyApiClient())
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    final public function handle(): int
    {
        $response = $this->apiClient->getUserList();
        $content = json_decode($response->getBody()->getContents(), true);
        foreach ($content['data'] as $item) {
            if (!User::where(User::EXTERNAL_ID, $item['id'])->first()) {
                $user = new User;
                $user->{User::EXTERNAL_ID} = $item['id'];
                $user->{User::ABOUT} = $item['title'];
                $user->{User::FIRSTNAME} = $item['firstName'];
                $user->{User::LASTNAME} = $item['lastName'];

                $user->save();
            }
        }

        return 0;
    }
}
