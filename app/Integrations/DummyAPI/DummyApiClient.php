<?php

namespace App\Integrations\DummyAPI;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class DummyApiClient
{
    use DummyApiClientExtension;

    private Client $client;

    private array $headers;

    private array $query;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => 'https://dummyapi.io/data/v1/']);
        $this->headers = ['app-id' => env('DUMMY_API_KEY')];
        $this->query = ['page' => 0, 'limit' => env('DUMMY_API_LIMIT')];
    }

    private function sendRequest(string $method, string $uri, array $options = []): ResponseInterface
    {
        $options['headers'] = isset($options['headers'])
            ? array_merge($options['headers'], $this->headers)
            : $this->headers;

        $options['query'] = [
            'page' => isset($options['query']['page']) ? $options['query']['page'] : $this->query['page'],
            'limit' => isset($options['query']['limit']) ? $options['query']['limit'] : $this->query['limit'],
        ];

        return $this->client->request(
            method: $method,
            uri: $uri,
            options: $options,
        );
    }

    final public function getUserList(int $page = 0): ResponseInterface
    {
        $options = ['query' => ['page' => $page]];

        return $this->sendRequest(method: 'GET', uri: 'user', options: $options);
    }

    final public function getUserById(int|string $id): ResponseInterface
    {
        return $this->sendRequest(method: 'GET', uri: "user/$id");
    }

    final public function createUser(string $firstName, string $lastName, string $email): ResponseInterface
    {
        $body = json_encode([
            'firstName' => $firstName,
            'lastName' => $lastName,
            'email' => $email,
        ], true);

        return $this->sendRequest(method: 'POST', uri: 'user/create', options: ['body' => $body]);
    }

    final public function updateUser(
        string $id,
        ?string $email,
        ?string $title,
        ?string $firstName,
        ?string $lastName
    ): ResponseInterface
    {
        $body = json_encode([
            'email' => $email,
            'title' => $title,
            'firstName' => $firstName,
            'lastName' => $lastName,
        ], true);

        return $this->sendRequest(method: 'PUT', uri: "user/$id", options: ['body' => $body]);
    }

    final public function deleteUser(string $id): ResponseInterface
    {
        return $this->sendRequest(method: 'DELETE', uri: "user/$id");
    }
}
