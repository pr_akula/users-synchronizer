<?php

namespace App\Integrations\DummyAPI;

use Psr\Http\Message\ResponseInterface;

trait DummyApiClientExtension
{
    final public static function stringifyResponse(ResponseInterface $response): string
    {
        return $response->getBody()->getContents();
    }
}
