<?php

namespace App\Enums\JobSystem;

enum CommandStatus: string
{
    case ADDED              = 'ADDED';
    case IN_PROCESS         = 'IN_PROCESS';
    case DATA_STORED        = 'DATA_STORED';
    case DATA_NOT_RECEIVED  = 'DATA_NOT_RECEIVED';
    case DATA_HANDLED       = 'DATA_HANDLED';
}
