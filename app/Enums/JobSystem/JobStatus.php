<?php

namespace App\Enums\JobSystem;

enum JobStatus: string
{
    case ADDED      = 'ADDED';
    case IN_PROCESS = 'IN_PROCESS';
    case COMPLETED  = 'COMPLETED';
    case FAILED     = 'FAILED';
    case DECLINED   = 'DECLINED';
    case STOPPED    = 'STOPPED';
}
