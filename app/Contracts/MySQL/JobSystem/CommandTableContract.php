<?php

namespace App\Contracts\MySQL\JobSystem;

interface CommandTableContract
{
    public const TABLE_NAME                 = 'commands';

    public const PRIMARY_KEY                = self::ID;
    public const JOB_FOREIGN_KEY            = self::JOB_ID;

    public const ID                         = 'id';
    public const JOB_ID                     = 'job_id';
    public const QUEUE_NUMBER               = 'queue_number';
    /**
     * Command::EXECUTE_CONFIG - is the array with structure:
     * [
     *     'run_class' => <...>,
     *     'run_method' => <...>,
     *     'run_method_parameters' => <...>,
     *
     *     'prepare_result_class' => <...>,
     *     'prepare_result_method' => <...>,
     * ]
     */
    public const EXECUTE_CONFIG             = 'execute_config';
    public const STATUS                     = 'status';
    public const RESPONSE_DATA              = 'response_data';
    public const ERROR_MESSAGE              = 'error_message';
}
