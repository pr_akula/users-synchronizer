<?php

namespace App\Contracts\MySQL\JobSystem;

interface JobTableContract
{
    public const TABLE_NAME                 = 'jobs';

    public const PRIMARY_KEY                = self::ID;

    public const ID                         = 'id';
    public const NAME                       = 'name';
    public const DESCRIPTION                = 'description';
    public const STATUS                     = 'status';
}
